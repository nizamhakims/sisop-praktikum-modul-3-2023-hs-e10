#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

void download(char link[]){
  pid_t child_id;

  child_id = fork();

  if (child_id == -1) {
    exit(EXIT_FAILURE);
  }
  
  if (child_id == 0) {

	char namaZip[] = "hehe.zip";
    char *argv[] = {"wget", "-O", namaZip, link, NULL};
    execvp("wget", argv);

  }
  int status;
  wait(&status);
}

void unzip(char namaZip[]){
  pid_t child_id;

  child_id = fork();

  if (child_id == -1) {
    exit(EXIT_FAILURE);
  }
  
  if (child_id == 0) {

    char *argv[] = {"unzip", namaZip, NULL};
    execvp("unzip", argv);

  }
  int status;
  wait(&status);
}

int main(){
	download("https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download");
	unzip("hehe.zip");
}
