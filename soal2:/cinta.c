#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5
#define SHM_SIZE ROWS1 *COLS2 * sizeof(int)

int i, j;

// factorial
void *factorial(void *arg)
{
    int num;
    int *val = (int *)arg;

    for (i = 0; i < ROWS1; i++)
    {
        for (j = 0; j < COLS2; j++)
        {
            num = val[i * COLS2 + j];
            unsigned long long ans = 1, k;
            for (k = 2; k <= num; k++)
                ans *= k;
            val[i * COLS2 + j] = ans;
        }
    }
    pthread_exit(NULL);
}

int main()
{
    // execution time
    clock_t start_time, end_time;
    double cpu_time_used;

    int shmid;
    key_t key = 1234;
    int *shm, *s;
    unsigned long long ans[ROWS1][COLS2];

    // shared memory of matrix multiplication
    if ((shmid = shmget(key, SHM_SIZE, 0666)) < 0)
    {
        perror("shmget");
        exit(1);
    }
    if ((shm = shmat(shmid, NULL, 0)) == (int *)-1)
    {
        perror("shmat");
        exit(1);
    }
    s = shm;
    for (i = 0; i < ROWS1; i++)
    {
        for (j = 0; j < COLS2; j++)
            ans[i][j] = *(s + i * COLS2 + j);
    }

    // display matrix multiplication
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROWS1; i++)
    {
        for (j = 0; j < COLS2; j++)
        {
            printf("%d ", *(s + i * COLS2 + j));
        }
        printf("\n");
    }

    // thread for calculate factorial
    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    start_time = clock(); // start timing
    pthread_create(&tid, &attr, factorial, (void *)s);
    pthread_join(tid, NULL);
    end_time = clock(); // end timing

    // display factorial
    printf("Matriks faktorial:\n");
    for (i = 0; i < ROWS1; i++)
    {
        for (j = 0; j < COLS2; j++)
            printf("%llu ", (unsigned long long)*(s + i * COLS2 + j));
        printf("\n");
    }

    // release shared memorys
    shmdt(shm);
    shmctl(shmid, IPC_RMID, NULL);

    // display execution time
    cpu_time_used = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu eksekusi: %f detik\n", cpu_time_used);

    return 0;
}
