#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

// matrix 1
#define ROWS1 4
#define COLS1 2
// matrix 2
#define ROWS2 2
#define COLS2 5
// determines size of shared memory to store result of multiplication matrix
#define SHM_SIZE ROWS1 *COLS2 * sizeof(int)

int main()
{
    int i, j, k, matrix1[ROWS1][COLS1], matrix2[ROWS2][COLS2], ans[ROWS1][COLS2];

    // input 1st matrix ussing rand() function
    srand(time(NULL));
    for (i = 0; i < ROWS1; i++)
        for (j = 0; j < COLS1; j++)
            matrix1[i][j] = rand() % 5 + 1;

    // input 2nd matrix using rand() function
    for (i = 0; i < ROWS2; i++)
        for (j = 0; j < COLS2; j++)
            matrix2[i][j] = rand() % 4 + 1;

    // print 1 st matrix
    printf("Matrix 1:\n");
    for (i = 0; i < ROWS1; i++)
    {
        for (j = 0; j < COLS1; j++)
            printf("%d ", matrix1[i][j]);
        printf("\n");
    }

    // print 2 nd matrix
    printf("Matrix 2:\n");
    for (i = 0; i < ROWS2; i++)
    {
        for (j = 0; j < COLS2; j++)
            printf("%d ", matrix2[i][j]);
        printf("\n");
    }

    // matrix multiplication
    for (i = 0; i < ROWS1; i++)
        for (j = 0; j < COLS2; j++)
        {
            ans[i][j] = 0;
            for (k = 0; k < COLS1; k++)
                ans[i][j] += matrix1[i][k] * matrix2[k][j];
        }

    // shared memory for copying matrix multiplication
    int shmid;
    key_t key = 1234;
    int *shm, *s;
    if ((shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666)) < 0)
    {
        perror("shmget");
        exit(1);
    }
    if ((shm = shmat(shmid, NULL, 0)) == (int *)-1)
    {
        perror("shmat");
        exit(1);
    }
    s = shm;
    for (i = 0; i < ROWS1; i++)
    {
        for (j = 0; j < COLS2; j++)
            *s++ = ans[i][j];
    }

    // display matrix multiplication
    printf("matrix multiplication :\n");
    for (i = 0; i < ROWS1; i++)
    {
        for (j = 0; j < COLS2; j++)
            printf("%d ", ans[i][j]);
        printf("\n");
    }

    // release shared memory
    shmdt(shm);

    return 0;
}
