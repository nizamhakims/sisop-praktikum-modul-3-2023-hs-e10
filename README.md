# Lapres Praktikum Modul 3

## Soal 1
## Soal 2
a. Membuat program C dengan nama kalian.c yang berisi program untuk melakukan perkalian matriks.
1. Pendefinisian ukuran matriks pertama dan kedua menggunakan macro ROWS1, COLS1, ROWS2, dan COLS2.
2. Pendefinisian ukuran shared memory yang digunakan untuk menyimpan hasil perkalian matriks menggunakan macro SHM_SIZE yang didefinisikan sebagai ROWS1 * COLS2 * sizeof(int).
3. Inisialisasi seed untuk fungsi rand() menggunakan waktu saat program dijalankan dengan memanggil fungsi srand(time(NULL)).
4. Input matriks pertama dan kedua dengan nilai acak menggunakan fungsi rand() dengan batasan nilai 0 sampai dengan 3.
5. Print matriks pertama dan kedua dengan perulangan for.
6. Lakukan operasi perkalian matriks dengan menggunakan perulangan for, di mana matriks hasil perkalian akan disimpan pada array bernama ans.
7. Membuat shared memory dengan ukuran SHM_SIZE dan key 1234, lalu memasukkan nilai hasil perkalian matriks ke dalam shared memory.
8. Menampilkan matriks hasil perkalian pada konsol dengan perulangan for.
9. Melepas shared memory.

b. Membuat program C kedua dengan nama cinta.c yang akan mengambil variabel hasil perkalian matriks dari program kalian.c untuk mencari nilai faktorialnya dengan menggunakan thread.
- Fungsi factorial yang akan dijalankan sebagai thread untuk menghitung faktorial setiap elemen matriks. 
Fungsi ini akan mengambil argumen berupa pointer ke shared memory yang berisi matriks yang akan dihitung faktorialnya. Setelah faktorial selesai dihitung, nilai hasil faktorial akan disimpan kembali ke shared memory.

- Fungsi main :
1. Inisialisasi beberapa konstanta dan variabel.
2. Membuat shared memory dengan memanggil fungsi shmget(). Jika fungsi ini gagal, program keluar dengan pesan kesalahan. Jika shared memory berhasil dibuat, program melampirkan shared memory ke prosesnya menggunakan shmat().
3. Mengambil elemen dari shared memory dan menyimpannya di array ans menggunakan loop for untuk mengulang sebanyak ROWS1 dan COLS2. Loop for ini mengambil setiap elemen dari shared memory dan menempatkannya di posisi yang sesuai dalam array ans.
4. Menampilkan matriks hasil perkalian dengan menggunakan loop for untuk mengulang sebanyak ROWS1 dan COLS2. Setiap elemen dari matriks hasil perkalian diambil dari shared memory dan ditampilkan ke layar.
4. Membuat thread untuk menghitung faktorial matriks dengan menggunakan fungsi pthread_create(). Variabel tid menyimpan ID dari thread yang baru saja dibuat, dan attr digunakan untuk menentukan sifat thread (misalnya, apakah bersifat joinable atau detached).
5. Fungsi factorial digunakan sebagai fungsi yang akan dijalankan oleh thread yang baru saja dibuat. Parameter s yang di-passing pada factorial digunakan untuk menunjuk ke matriks yang akan dihitung faktorialnya.
6. Setelah thread selesai dijalankan, fungsi pthread_join() dipanggil untuk menunggu thread selesai bekerja. Setelah itu, matriks faktorial akan ditampilkan pada layar menggunakan perulangan for
7. Melepaskan shared memory dengan memanggil shmdt() dan shmctl() untuk membebaskan sumber daya yang digunakan oleh shared memory.
8. Mengukur waktu eksekusi dari awal hingga akhir program dengan menggunakan fungsi clock(). Ini dilakukan dengan memanggil fungsi clock().
9. Program menampilkan hasil faktorial dan waktu eksekusi.

c. Membuat program C ketiga dengan nama sisop.c yang akan mengambil variabel hasil perkalian matriks dari program kalian.c untuk mencari nilai faktorialnya tanpa thread.
1. Inisialisasi beberapa konstanta dan variabel.
2. Membuat shared memory dengan memanggil fungsi shmget(). Jika fungsi ini gagal, program keluar dengan pesan kesalahan. Jika shared memory berhasil dibuat, program melampirkan shared memory ke prosesnya menggunakan shmat().
3. Mengambil elemen dari shared memory dan menyimpannya di array ans menggunakan loop for untuk mengulang sebanyak ROWS1 dan COLS2. Loop for ini mengambil setiap elemen dari shared memory dan menempatkannya di posisi yang sesuai dalam array ans.
4. Menampilkan matriks hasil perkalian dengan menggunakan loop for untuk mengulang sebanyak ROWS1 dan COLS2. Setiap elemen dari matriks hasil perkalian diambil dari shared memory dan ditampilkan ke layar.
5. Menghitung faktorial dari setiap elemen matriks hasil perkalian. Ini dilakukan dengan menggunakan loop for untuk mengulang sebanyak ROWS1 dan COLS2. Loop for ini mengambil setiap elemen dari array ans dan menghitung faktorialnya.
6. Melepaskan shared memory dengan memanggil shmdt() dan shmctl() untuk membebaskan sumber daya yang digunakan oleh shared memory.
7. Mengukur waktu eksekusi dari awal hingga akhir program dengan menggunakan fungsi clock(). Ini dilakukan dengan memanggil fungsi clock() di awal dan di akhir program dan menghitung selisihnya.
8. Program menampilkan hasil faktorial dan waktu eksekusi.

![](Dokumentasi/cinta.jpg)
![](Dokumentasi/sisop.jpg)
## Soal 3
## Soal 4

### 1. unzip.c
#### download()
- Fungsi download() digunakan untuk mendownload file zip dengan mengimplementasikan fork() untuk membuat child process
- Pada child process, download dilakukan dengan execvp dan wget menggunakan link yang tersedia pada "Soal Shift Modul 3"
- File zip disimpan dengan nama hehe.zip

```c
void download(char link[]){
  pid_t child_id;

  child_id = fork();

  if (child_id == -1) {
    exit(EXIT_FAILURE);
  }
  
  if (child_id == 0) {

	char namaZip[] = "hehe.zip";
    char *argv[] = {"wget", "-O", namaZip, link, NULL};
    execvp("wget", argv);

  }
  int status;
  wait(&status);
}
```

#### unzip()
- Setelah berhasil didownload, selanjutnya kita lakukan unzip pada hehe.zip
- Dengan menggunakan fork() untuk membuat child process dan execvp dan unzip file akan diexract ke current folder 

```c
void unzip(char namaZip[]){
  pid_t child_id;

  child_id = fork();

  if (child_id == -1) {
    exit(EXIT_FAILURE);
  }
  
  if (child_id == 0) {

    char *argv[] = {"unzip", namaZip, NULL};
    execvp("unzip", argv);

  }
  int status;
  wait(&status);
}
```

#### main()

```c
int main(){
    download("https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download");
    unzip("hehe.zip");
}
```

![](Dokumentasi/unzip.jpg)

### 2. categorize.c
#### struct Extension
- Ext digunakan untuk meyimpan daftar nama extension dari "extension.txt" dan banyaknya file dengan extension tersebut pada folder "files"
```c
typedef struct Extension{
    char name[10];
    int ct_file;
}
Ext;
```
#### global variable
- extList = array untuk menyimpan masing-masing Ext
- ct_ext = counter untuk menghitung banyak extension pada "extension.txt"
- ct_other = counter untuk menghitung banyak file berkategori "other"
- max_folder_size = untuk menyimpan isi max dari sebuah folder dari "max.txt"
```c
Ext extList[100];
int ct_ext;
int ct_other;
int max_folder_size;
```

#### get_timestamp()
- Mendapatkan waktu sekarang untuk keperluan pembuatan log
```c
char *get_timestamp(){
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);
    char *timestamp = (char *) malloc(20 * sizeof(char));

    strftime(timestamp, 20, "%d-%m-%Y %H:%M:%S", tm_info);
    return timestamp;
}
```

#### write_log()
- Menuliskan log pada log.txt
- Menerapkan fungsi get_timestamp() untuk menuliskan waktu dibuatnya log tersebut
```c
void write_log(char *log){
    FILE *fp = fopen("log.txt", "a");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    fprintf(fp, "%s %s\n", get_timestamp(), log);
    fclose(fp);
}
```

#### make_dir()
- Membuat folder baru dengan menerapkan fork(), execv, dan mkdir
- Menerima argumen char* yang akan digunakan sebagai nama folder
- Apabila pembuatan folder berhasil maka akan menambahkan log "MADE" pada "log.txt" menggunakan fungsi write_log()
```c
void make_dir(char *dirname){
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat folder %s\n", dirname);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan folder %s tidak berhenti dengan normal\n", dirname);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MADE %s", dirname + 2);
    write_log(log);
}
```

#### make_file()
- Membuat txt file dengan menerapkan fork(), execv, dan touch
- Digunakan untuk membuat log.txt
```c
void make_file(char *filename){
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"touch", filename, NULL};
        execv("/usr/bin/touch", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
}
```

#### move_file()
- Menggunakan fork(), execv, dan mv untuk memindahkan suatu file dalam folder "files" ke folder sesuai dengan extensionnya atau ke folder other
- Apabila pemindahan file berhasil maka akan menambahkan log "MOVED" pada "log.txt" menggunakan fungsi write_log()
```c
void move_file(char *filename, char *dest, char *ext){
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal memindahkan file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mv", filename, dest, NULL};
        execv("/usr/bin/mv", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pemindahan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MOVED %s file : %s > %s", ext, filename, dest + 2);
    write_log(log);
}
```

#### check_extension()
- Digunakan untuk mengecek apakah file yang sedang kita cek memiliki extension yang sama dengan salah satu extension pada "extension.txt"

```c
bool check_extension(char *filename, char *ext){
    int len_file = strlen(filename);
    int len_ext = strlen(ext);

    if(len_file <= len_ext){
        return false;
    }
    char temp[len_ext + 1];
    strcpy(temp, filename + len_file - len_ext);

    for(int i = 0; i < len_ext; ++i){
        if(isalpha(temp[i]))
            temp[i] = tolower(temp[i]);
    }
    return (strcmp(temp, ext) == 0);
}
```
#### read_files()
- Digunakan untuk membaca file "extension.txt" dan "max.txt" untuk mendapatkan variable-variable yang diperlukan didalamnya
- Hasil read "extension.txt" disimpan dalam extList
- Hasil read "max.txt" disimpan dalam max_folder_size
```c
void read_files(){
    char buffer[10];
    FILE *fp;
    fp = fopen("extensions.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file extensions.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
    {
        if(buffer[strlen(buffer) - 1] == '\n')
            buffer[strlen(buffer) - 1] = '\0';
        strcpy(extList[ct_ext].name, buffer);
        ct_ext++;
    }
    for(int i = 0; i < ct_ext; ++i){
        extList[i].name[strlen(extList[i].name) - 1] = '\0';
    }
    fp = fopen("max.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file max.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
        max_folder_size = atoi(buffer);
    fclose(fp);
}
```
#### make_folders()
- Digunakan untuk inisiasi folder "categorized", "other", dan folder-folder extension awal
```c
void make_folders(){
    make_dir("./categorized");
    for(int i = 0; i < ct_ext; ++i){
        char path[1000];
        sprintf(path, "./categorized/%s", extList[i].name);
        make_dir(path);
    }
    make_dir("./categorized/other");
}
```

#### categorizing()
- Fungsi utama dalam categorize.c
- Digunakan untuk mengecek tiap-tiap entri file dalam folder "files"
- Apabila file berupa folder maka akan dilakukan pengecekan secara rekursif ke dalam folder tersebut karena yang ingin dipindahkan hanya file yang berupa regular file
- Setiap pengecekan folder akan membuat thread baru untuk melakukan tugas tersebut
- Setiap pengecekan folder akan menuliskan log "ACCESSED" pada "log.txt"
- Apabila file berupa regular file maka akan dicek apakah extension file tersebut ada pada extList
- Apabila ada maka file akan dipindahkan ke dalam folder yang sesuai, tetapi jika folder sudah memenuhi batas kapasitas maka akan dibuat folder baru menggunakan fungsi make_dir()
- Apabila tidak ada maka akan dipindahkan dalam folder "other"
- Pemindahan file menggunakan fungsi move_file()

```c
void *categorizing(void *args){
    char *path = (char *) args;
    DIR *dir = opendir(path);
    struct dirent *ent;

    if(dir == NULL){
        printf("Gagal membuka direktori %s\n", path);
        pthread_exit(NULL);
    }
    while((ent = readdir(dir)) != NULL)
    {
        if(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){
            continue;
        }
        if(ent->d_type == DT_DIR)
        {
            char sub_dir[1000], log[2000];
            sprintf(sub_dir, "%s/%s", path, ent->d_name);

            sprintf(log, "ACCESSED %s", sub_dir);
            write_log(log);

            pthread_t thread;
            pthread_create(&thread, NULL, categorizing, (void *) sub_dir);
            pthread_join(thread, NULL);
        }
        else if(ent->d_type == DT_REG)
        {
            bool other = true;
            char file_path[1000];
            sprintf(file_path, "%s/%s", path, ent->d_name);

            for(int i = 0; i < ct_ext; ++i)
            {
                if(check_extension(ent->d_name, extList[i].name))
                {
                    char dest[1000];
                    extList[i].ct_file++;
                    int idx_folder = floor(extList[i].ct_file / max_folder_size);

                    if(idx_folder == 0)
                        sprintf(dest, "./categorized/%s", extList[i].name);
                    else {
                        sprintf(dest, "./categorized/%s (%d)", extList[i].name, idx_folder + 1);
                        if(access(dest, F_OK) != 0)
                            make_dir(dest);
                    }
                    move_file(file_path, dest, extList[i].name);
                    other = false;
                    break;
                }
            }
            if(other){
                move_file(file_path, "./categorized/other", "other");
                ct_other++;
            }
        }
    }
    closedir(dir);
    pthread_exit(NULL);
}
```

#### swap_Ext()
- Untuk menukar 2 struct extension
```c
void swap_Ext(Ext *a, Ext *b){
    Ext temp = *a;
    *a = *b;
    *b = temp;
}
```
#### sort_Ext()
- Mengurutkan extension secara ascending berdasarkan banyaknya file yang memiliki extension tersebut
```c
void sort_Ext(){
    for(int j = ct_ext - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(extList[i].ct_file > extList[i + 1].ct_file)
                swap_Ext(extList + i, extList + i + 1);
        }
    }
}
```

#### print_Ext()
Menampilkan banyaknya file dari masing-masing jenis extension dan dari folder other
```c
void print_Ext(){
    for(int i = 0; i < ct_ext; ++i)
        printf("extension_%-5s : %d\n", extList[i].name, extList[i].ct_file);
    printf("%-15s : %d\n", "other", ct_other);
}
```

#### main()
```c
int main(){
    read_files();
    make_folders();
    make_file("log.txt");

    char path[1000] = "files";
    pthread_t thread;
    pthread_create(&thread, NULL, categorizing, (void *) path);
    pthread_join(thread, NULL);

    sort_Ext();
    print_Ext();
    return 0;
}
```
![](Dokumentasi/categorize.jpg)
